defmodule BottlesOfBeer do
  @spec line(integer()) :: binary()
  def line(bottles) when is_integer(bottles) and bottles > 0 do
    """
    #{bottles} #{bottle(bottles)} of beer on the wall, #{bottles} #{bottle(bottles)} of beer.
    Take one down and pass it around, #{count_bottles(bottles - 1)} #{bottle(bottles - 1)} of beer on the wall.
    """
  end

  def line(bottles) when is_integer(bottles) and bottles == 0 do
    """
    No more bottles of beer on the wall, no more bottles of beer.
    Go to the store and buy some more, 99 bottles of beer on the wall.
    """
  end

  @spec count_bottles(integer()) :: binary()
  defp count_bottles(0), do: "no more"
  defp count_bottles(bottles) when is_integer(bottles), do: "#{bottles}"

  @spec bottle(integer()) :: binary()
  defp bottle(1), do: "bottle"
  defp bottle(bottles) when is_integer(bottles), do: "bottles"

  @spec sing_a_song :: binary()
  def sing_a_song do
    99..0//-1
    |> Enum.to_list()
    |> Enum.reduce("", fn i, acc -> acc <> line(i) end)
  end
end
