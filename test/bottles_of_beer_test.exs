defmodule BottlesOfBeerTest do
  use ExUnit.Case
  doctest BottlesOfBeer

  describe "BottlesOfBeer.line/1" do
    test "prints correct message when 0 bottles remain" do
      assert "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n" ==
               BottlesOfBeer.line(0)
    end

    test "prints correct message when 1 bottle remains" do
      assert "1 bottle of beer on the wall, 1 bottle of beer.\nTake one down and pass it around, no more bottles of beer on the wall.\n" ==
               BottlesOfBeer.line(1)
    end

    test "prints correct message when more than 1 bottle remains" do
      assert "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n" ==
               BottlesOfBeer.line(2)
    end
  end
end
